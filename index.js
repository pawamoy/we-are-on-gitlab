/**
 * This is the main entrypoint to your Probot app
 * @param {import('probot').Application} app
 */
module.exports = app => {
  // Your code here
  app.log('App loaded')

  app.on('issues.opened', async context => {
    const issue = context.payload.issue
    const repository = context.payload.repository
    const uriTitle = encodeURIComponent(issue.title)
    const uriDescription = encodeURIComponent(issue.body)
    const issueTextLines = [
      `Beep boop, hi @${issue.user.login}, `,
      `thank you for opening this issue :slightly_smiling_face:\n\n`,
      `However, the development of ${repository.name} happens `,
      `[on GitLab](https://gitlab.com/${repository.full_name}) :confused:\n\n`,
      `Fortunately enough, here is a link to open a new issue on GitLab `,
      `with pre-filled values: [just click here!]`,
      `(https://gitlab.com/${repository.full_name}/issues/new?`,
      `issue[title]=${uriTitle}&`,
      `issue[description]=${uriDescription}) :bowtie:`
    ]
    const issueComment = context.issue({
      body: issueTextLines.join("")
    })
    return context.github.issues.createComment(issueComment)
  })

  // For more information on building apps:
  // https://probot.github.io/docs/

  // To get your app running against GitHub, see:
  // https://probot.github.io/docs/development/
}
